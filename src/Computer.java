import java.util.Random;

class Computer extends Player {
    Random rnd;


    public Computer() {
        super();
        rnd = new Random();
        this.setName("unNamed" + rnd.nextInt(99));
    }

    public Computer(String name) {
        super(name);
    }

    public void setMovefunc() {
        this.setMove(rnd.nextInt(3));
        System.out.print("");
    }

}
