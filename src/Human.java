import java.util.Scanner;

class Human extends Player {
    Scanner scanner;

    public Human() {
        super();
        scanner = new Scanner(System.in);
        System.out.println("Введите ваше имя\n");
        this.setName(scanner.nextLine());
    }

    public void setMovefunc() {
        System.out.println("\nВведите ваш ход. (Только число)\n\n1. Камень\n2. Ножницы\n3. Бумага\n");
        int v = scanner.nextInt();
        if (v > 3 || v < 1) {
            System.out.println("\nВвеcти можно только цифру от 1 до 3!");
            this.setMovefunc();
        } else
            this.setMove(v - 1);
    }
}
