abstract class Player {
    private String name;
    private int move;
    private int points;

    public Player() {
        this.name = null;
        this.move = 0;
        this.points = 0;
    }

    public Player(String name) {
        this.name = name;
        this.move = 0;
        this.points = 0;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMove() {
        return move;
    }

    public void addPoint() {
        this.points++;
    }

    public int getPoints() {
        return this.points;
    }

    public String getMoveName() {
        String s = this.getMove() == 0 ? "Камень" : (this.getMove() == 1 ? "Ножницы" : "Бумага");
        return s;
    }

    public abstract void setMovefunc();

    public void setMove(int a) {
        this.move = a;
    }
}
