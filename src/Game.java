import java.util.Scanner;

class Game {
    private Player p1;
    private Player p2;
    Scanner scanner = new Scanner(System.in);


    public Game(Player p1, Player p2) {
        this.p1 = p1;
        this.p2 = p2;
    }

    void play() {
        p1.setMovefunc();
        p2.setMovefunc();
        winDecision();
        showStats();
        System.out.println();
        newGame();
    }

    void newGame() {
        System.out.println("Хотите сыграть еще раз? +/-\n");
        String s = scanner.nextLine();
        if (s.equals("y") || s.equals("Y") || s.equals("+"))
            play();
        else if (s.equals("N") || s.equals("n") || s.equals("-"))
            System.exit(0);
        else {
            System.out.println("Вы ввели неверный символ! Введите либо \"+\" либо \"-\".\n");
            newGame();
        }
    }


    void winDecision() {
        int move1 = p1.getMove();
        int move2 = p2.getMove();
        System.out.printf("%10s\t:\t%-10s\n\n", p1.getMoveName(), p2.getMoveName());
        if (move1 == 0) {
            switch (move2) {
                case 0:
                    System.out.println("Ничья!");
                    break;
                case 1:
                    System.out.println(p1.getName() + " выйграл этот раунд!");
                    p1.addPoint();
                    break;
                case 2:
                    System.out.println(p2.getName() + " выйграл этот раунд!");
                    p2.addPoint();
                    break;
            }
        }
        if (move1 == 1) {
            switch (move2) {
                case 0:
                    System.out.println(p2.getName() + " выйграл этот раунд!");
                    p2.addPoint();
                    break;
                case 1:
                    System.out.println("Ничья!");
                    break;
                case 2:
                    System.out.println(p1.getName() + " выйграл этот раунд!");
                    p1.addPoint();
                    break;
            }
        }
        if (move1 == 2) {
            switch (move2) {
                case 0:
                    System.out.println(p1.getName() + " выйграл этот раунд!");
                    p1.addPoint();
                    break;
                case 1:
                    System.out.println(p2.getName() + " выйграл этот раунд!");
                    p2.addPoint();
                    break;
                case 2:
                    System.out.println("Ничья!");
                    break;
            }
        }
    }

    void showStats() {
        System.out.println("\nСчет игры.");
        System.out.printf("\n%10s : %-10s\n", p1.getName(), p2.getName());
        System.out.printf("\n%10d : %-10d\n", p1.getPoints(), p2.getPoints());
    }
}